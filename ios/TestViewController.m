//
//  RNViewController.m
//  test
//
//  Created by hj on 2018/11/23.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "TestViewController.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

@implementation TestViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.title = @"这是原生页面";
  
  UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(100, 100, 200, 50)];
  [button setTitle:@"返回" forState:UIControlStateNormal];
  [button addTarget:self action:@selector(onClickButton) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:button];
}


- (void)onClickButton {
  [self.navigationController popViewControllerAnimated:YES];
}

@end
