//
//  OpenNativeModule.m
//  test
//
//  Created by hj on 2018/11/23.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "PushNative.h"
// 导入AppDelegate，获取UINavigationController
#import "AppDelegate.h"
// 导入跳转的页面
#import "TestViewController.h"

@implementation PushNative

RCT_EXPORT_MODULE();
// RN跳转原生界面
// RNOpenOneVC指的就是跳转的方法，下面会用到
RCT_EXPORT_METHOD(rnToNative) {
  //主要这里必须使用主线程发送,不然有可能失效
  dispatch_async(dispatch_get_main_queue(), ^{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *rootNav = app.navController;

    TestViewController *test = [[TestViewController alloc] init];
    
    [rootNav pushViewController:test animated:YES];
  });
}

@end
