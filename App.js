/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, Platform, Text, NativeModules, View, TouchableOpacity } from 'react-native';


export default class App extends Component {

  _goTo = () => {
    if (Platform.OS === 'ios') {
      NativeModules.PushNative.rnToNative();
    } else {
      NativeModules
        .IntentModule
        .startActivityFromJS("com.test.TestActivity", null);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this._goTo}>
          <Text>这是RN页面,点击跳转原生界面</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
